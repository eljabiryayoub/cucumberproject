Feature: Server reply calculus

  Scenario Outline: Server reply calculus
    Given I call  on url  "/exp" with the number <number>
    And the response body must contain <exp>
    Examples:
      | number | exp |
      | 1 | 1 |
      | 2 | 4 |
      | 3 | 9 |
      | 4 | 16 |
      | 5 | 25 |
      | 6 | 36 |