$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("calculus.feature");
formatter.feature({
  "line": 1,
  "name": "Server reply calculus",
  "description": "",
  "id": "server-reply-calculus",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Server reply calculus",
  "description": "",
  "id": "server-reply-calculus;server-reply-calculus",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I call  on url  \"/exp\" with the number \u003cnumber\u003e",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the response body must contain \u003cexp\u003e",
  "keyword": "And "
});
formatter.examples({
  "line": 6,
  "name": "",
  "description": "",
  "id": "server-reply-calculus;server-reply-calculus;",
  "rows": [
    {
      "cells": [
        "number",
        "exp"
      ],
      "line": 7,
      "id": "server-reply-calculus;server-reply-calculus;;1"
    },
    {
      "cells": [
        "1",
        "1"
      ],
      "line": 8,
      "id": "server-reply-calculus;server-reply-calculus;;2"
    },
    {
      "cells": [
        "2",
        "4"
      ],
      "line": 9,
      "id": "server-reply-calculus;server-reply-calculus;;3"
    },
    {
      "cells": [
        "3",
        "9"
      ],
      "line": 10,
      "id": "server-reply-calculus;server-reply-calculus;;4"
    },
    {
      "cells": [
        "4",
        "16"
      ],
      "line": 11,
      "id": "server-reply-calculus;server-reply-calculus;;5"
    },
    {
      "cells": [
        "5",
        "25"
      ],
      "line": 12,
      "id": "server-reply-calculus;server-reply-calculus;;6"
    },
    {
      "cells": [
        "6",
        "36"
      ],
      "line": 13,
      "id": "server-reply-calculus;server-reply-calculus;;7"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 8,
  "name": "Server reply calculus",
  "description": "",
  "id": "server-reply-calculus;server-reply-calculus;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I call  on url  \"/exp\" with the number 1",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the response body must contain 1",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "/exp",
      "offset": 17
    },
    {
      "val": "1",
      "offset": 39
    }
  ],
  "location": "teststeps2.i_call_on_url_with_the_number(String,int)"
});
formatter.result({
  "duration": 218051293,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 31
    }
  ],
  "location": "teststeps2.the_response_body_must_contain(int)"
});
formatter.result({
  "duration": 1272944,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Server reply calculus",
  "description": "",
  "id": "server-reply-calculus;server-reply-calculus;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I call  on url  \"/exp\" with the number 2",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the response body must contain 4",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "/exp",
      "offset": 17
    },
    {
      "val": "2",
      "offset": 39
    }
  ],
  "location": "teststeps2.i_call_on_url_with_the_number(String,int)"
});
formatter.result({
  "duration": 2991802,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "4",
      "offset": 31
    }
  ],
  "location": "teststeps2.the_response_body_must_contain(int)"
});
formatter.result({
  "duration": 50686,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Server reply calculus",
  "description": "",
  "id": "server-reply-calculus;server-reply-calculus;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I call  on url  \"/exp\" with the number 3",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the response body must contain 9",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "/exp",
      "offset": 17
    },
    {
      "val": "3",
      "offset": 39
    }
  ],
  "location": "teststeps2.i_call_on_url_with_the_number(String,int)"
});
formatter.result({
  "duration": 2740614,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "9",
      "offset": 31
    }
  ],
  "location": "teststeps2.the_response_body_must_contain(int)"
});
formatter.result({
  "duration": 52612,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Server reply calculus",
  "description": "",
  "id": "server-reply-calculus;server-reply-calculus;;5",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I call  on url  \"/exp\" with the number 4",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the response body must contain 16",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "/exp",
      "offset": 17
    },
    {
      "val": "4",
      "offset": 39
    }
  ],
  "location": "teststeps2.i_call_on_url_with_the_number(String,int)"
});
formatter.result({
  "duration": 2944324,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "16",
      "offset": 31
    }
  ],
  "location": "teststeps2.the_response_body_must_contain(int)"
});
formatter.result({
  "duration": 50686,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Server reply calculus",
  "description": "",
  "id": "server-reply-calculus;server-reply-calculus;;6",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I call  on url  \"/exp\" with the number 5",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the response body must contain 25",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "/exp",
      "offset": 17
    },
    {
      "val": "5",
      "offset": 39
    }
  ],
  "location": "teststeps2.i_call_on_url_with_the_number(String,int)"
});
formatter.result({
  "duration": 2969026,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "25",
      "offset": 31
    }
  ],
  "location": "teststeps2.the_response_body_must_contain(int)"
});
formatter.result({
  "duration": 91429,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Server reply calculus",
  "description": "",
  "id": "server-reply-calculus;server-reply-calculus;;7",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "I call  on url  \"/exp\" with the number 6",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "the response body must contain 36",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "/exp",
      "offset": 17
    },
    {
      "val": "6",
      "offset": 39
    }
  ],
  "location": "teststeps2.i_call_on_url_with_the_number(String,int)"
});
formatter.result({
  "duration": 2684795,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "36",
      "offset": 31
    }
  ],
  "location": "teststeps2.the_response_body_must_contain(int)"
});
formatter.result({
  "duration": 71539,
  "status": "passed"
});
formatter.uri("get.feature");
formatter.feature({
  "line": 1,
  "name": "Server reply Hello World!",
  "description": "",
  "id": "server-reply-hello-world!",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 2,
  "name": "Server reply Hello World!",
  "description": "",
  "id": "server-reply-hello-world!;server-reply-hello-world!",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "I call GET on /",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "the response status is 200",
  "keyword": "Then "
});
formatter.step({
  "line": 5,
  "name": "the response body must contain message with Hello World!",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "/",
      "offset": 14
    }
  ],
  "location": "TestSteps.iCallGetOn(String)"
});
formatter.result({
  "duration": 24113110,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 23
    }
  ],
  "location": "TestSteps.theResponseStatusIs(int)"
});
formatter.result({
  "duration": 68973,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "message",
      "offset": 31
    },
    {
      "val": "Hello World!",
      "offset": 44
    }
  ],
  "location": "TestSteps.theResponseBodyMustContainFieldWithValue(String,String)"
});
formatter.result({
  "duration": 46837,
  "status": "passed"
});
});