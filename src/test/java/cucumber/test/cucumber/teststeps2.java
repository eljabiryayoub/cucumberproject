package cucumber.test.cucumber;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.Application;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by u957870 on 26/07/2017.
 */
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration
public class teststeps2 {
    @Autowired
    private TestRestTemplate restTemplate;

    private ResponseEntity<Integer> response;
    @Given("^I call  on url  \"([^\"]*)\" with the number (\\d+)$")
    public void i_call_on_url_with_the_number(String url, int arg2) throws Throwable {
        this.response = this.restTemplate.getForEntity(url+"/"+arg2, Integer.class, new String());

    }


    @Then("^the response body must contain (\\d+)$")
    public void the_response_body_must_contain(int arg1) throws Throwable {
        Assert.assertEquals(arg1, this.response.getBody().intValue());
    }
}
