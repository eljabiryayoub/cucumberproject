package cucumber.test.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format = {"html:src/test/resources/cucumber/"}, features = "src/test/resources/cucumber")
public class CucumberTest {

}
