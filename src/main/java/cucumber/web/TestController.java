package cucumber.web;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @RequestMapping(path = "/")
    public Response get() {
        return new Response("Hello World!");
    }

    @RequestMapping(path = "/exp/{i}", method = RequestMethod.GET)
    public int get(@PathVariable int i) {
        return i*i;
    }
}
